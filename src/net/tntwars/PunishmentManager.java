package net.tntwars;

import com.mysql.jdbc.PreparedStatement;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.tntwars.util.Epoch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class PunishmentManager implements Listener {
    /*
        --------------------------------------------------------------------------------------------------------------
        - UUID(1)          Type(2)          EndTime(3)          Timestamp(4)          Punisher(5)          Reason(6) -
        --------------------------------------------------------------------------------------------------------------
     */

    public static HashMap<ProxiedPlayer, String> muted = new HashMap<>();

    @EventHandler
    public void onJoin(PostLoginEvent event) {
        ProxiedPlayer player = event.getPlayer();
        UUID uuid = player.getUniqueId();

        try {
            PreparedStatement sql = (PreparedStatement) MySQL.con.prepareStatement("SELECT * FROM `PUNISH` WHERE UUID=?;");
            sql.setString(1, uuid.toString());
            ResultSet resultSet = sql.executeQuery();

            if (isInDatabase(resultSet)) {checkPunished(player, resultSet);}

            sql.close();
        } catch (Exception e) {e.printStackTrace();}
    }

    private boolean isInDatabase(ResultSet resultSet) {
        try {return resultSet.next();}
        catch (Exception e) {e.printStackTrace();}
        return false;
    }

    private void checkPunished(ProxiedPlayer player, ResultSet resultSet) {
        try {
            while (resultSet.next()) {
                String type = resultSet.getString("Type");
                int endTime = resultSet.getInt("EndTime");
                String punisher = resultSet.getString("Punisher");
                String reason = resultSet.getString("Reason");
                String time = timeFormat(endTime, "§f");

                if (endTime == 0) {return;}

                switch(type) {
                    case "ban":
                    case "tempban": {
                        player.disconnect("§8§m---------" + "\n" + "§cBanned by: §f" + punisher + "\n" + "§cReason: §f"
                                + reason + "\n" + "§cTime: §f" + time + "\n" + "§c§lAPPEAL:§9 http://tntwars.com/forums" + "\n" + "§8§m---------");
                        break;
                    }
                    case "mute":
                    case "tempmute": {
                        if (Epoch.getEpoch() < endTime) {time = timeFormat(endTime, "§f");}
                        muted.put(player, time);
                    }
                    default:
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void punish(UUID uuid, String type, int endTime, String punisher, String reason) {
        try {
            Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
            PreparedStatement newPunishment = (PreparedStatement) MySQL.con.prepareStatement("INSERT INTO `PUNISH` values(?,?,?,?,?,?);");

            newPunishment.setString(1, uuid.toString());
            newPunishment.setString(2, type);
            newPunishment.setInt(3, endTime);
            newPunishment.setTimestamp(4, timeStamp);
            newPunishment.setString(5, punisher);
            newPunishment.setString(6, reason);
            newPunishment.execute();

            switch(type) {
                case "ban":
                    ProxiedPlayer target = BungeeCord.getInstance().getPlayer(uuid);

                    target.disconnect("§8§m---------" + "\n" + "§cBanned by: §f" + punisher + "\n" + "§cReason: §f"
                            + reason + "\n" + "§cTime: §f" + endTime + "\n" + "§c§lAPPEAL:§9 http://tntwars.com/forums" + "\n" + "§8§m---------");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void unPunish(UUID uuid, String type) {
        BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {
            try{
                PreparedStatement sql = (PreparedStatement) MySQL.con.prepareStatement("SELECT * FROM `PUNISH` WHERE UUID=?;");
                sql.setString(1, uuid.toString());
                ResultSet resultSet = sql.executeQuery();
                while (resultSet.next()) {
                    if(resultSet.getString("Type").equals(type)){
                        sql.setInt(3, 0);
                    }
                }
                Main.mysql.update(sql);
            }catch(Exception e){e.printStackTrace();}
        });
    }

    public static void clearHistory(UUID uuid) {
        try{
            PreparedStatement sql = (PreparedStatement) MySQL.con.prepareStatement("DELETE * FROM `PUNISH` WHERE UUID=?;");
            sql.setString(1, uuid.toString());
            Main.mysql.update(sql);
        }catch(Exception e){e.printStackTrace();}
    }

    private static String timeFormat(int time, String color){
        if (time == -1){return color + "PERMANENT";}

        long days = TimeUnit.SECONDS.toDays(time);
        time -= TimeUnit.DAYS.toSeconds(days);

        long hours = TimeUnit.SECONDS.toHours(time);
        time -= TimeUnit.HOURS.toSeconds(hours);

        long minutes = TimeUnit.SECONDS.toMinutes(time);
        time -= TimeUnit.MINUTES.toSeconds(minutes);

        long seconds = TimeUnit.SECONDS.toSeconds(time);

        return color + timeFormat((int) days,(int) hours,(int) minutes,(int) seconds);
    }

    private static String timeFormat(int days, int hours, int minutes, int seconds) {
        String r = "";

        if (days != 0){
            if (days == 1){r += days + " day ";}
            else{r += days + " days ";}
        }
        if (hours != 0){
            if (hours == 1){r += hours + " hour ";}
            else{r += hours + " hours ";}
        }
        if (minutes != 0){
            if (minutes == 1){r += minutes + " minute ";}
            else{r += minutes + " minutes ";}
        }
        if (seconds != 0){
            if (seconds == 1){r += seconds + " second ";}
            else{r += seconds + " seconds ";}
        }

        r = r.substring(0,r.length()-1);
        return r;
    }
}
