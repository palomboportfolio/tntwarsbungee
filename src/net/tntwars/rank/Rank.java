package net.tntwars.rank;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.tntwars.MySQL;

import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings("ALL")
public class Rank {

    public static String getRankType1(ProxiedPlayer player) {
        try {
            ResultSet r = MySQL.con.createStatement().executeQuery(
                    "SELECT TYPE FROM Players WHERE UUID = '" + player.getUniqueId().toString() + "'");
            if (r.next()) {
                return r.getString("TYPE");
            }
            r.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getRankType2(ProxiedPlayer player) {
        try {
            ResultSet r = MySQL.con.createStatement().executeQuery(
                    "SELECT TYPE2 FROM Players WHERE UUID = '" + player.getUniqueId().toString() + "'");
            if (r.next()) {
                return r.getString("TYPE2");
            }
            r.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isDonor(ProxiedPlayer player) {
        assert getRankType1(player) != null;
        return !getRankType1(player).equalsIgnoreCase("DEFAULT");
    }

    public static boolean isStaff(ProxiedPlayer player) {
        assert getRankType2(player) != null;
        return !getRankType2(player).equalsIgnoreCase("NONE");
    }

    @SuppressWarnings("static-access")
    public static boolean canDoDonor(ProxiedPlayer player, RankType1 rank, boolean inform) {
        if (rank.valueOf(getRankType1(player)).compareTo(rank) <= 0) {
            return true;
        }
        if (inform) {
            player.sendMessage("§c§lPermissions §7You must have at least §8[§c§l" + rank + "§8]§7.");
        }

        return false;
    }

    @SuppressWarnings("static-access")
    public static boolean canDoStaff(ProxiedPlayer player, RankType2 rank, boolean inform) {
        if (rank.valueOf(getRankType2(player)).compareTo(rank) <= 0) {
            return true;
        }
        if (inform) {
            player.sendMessage("§c§lPermissions §7You must have at least §8[§c§l" + rank + "§8]§7.");
        }

        return false;
    }
}
