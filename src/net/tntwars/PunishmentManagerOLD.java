package net.tntwars;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.tntwars.util.Epoch;
import net.tntwars.util.PlayerUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class PunishmentManagerOLD {
	public static String bar = Main.color("&8&m---------------------------------------");
	public static ArrayList<UUID> muted = new ArrayList<>();
	public static HashMap<UUID, Integer> mute_expire = new HashMap<>();
	public static HashMap<UUID, String> mute_reason = new HashMap<>();
	public static HashMap<UUID, String> muted_by = new HashMap<>();
	public static void join(ProxiedPlayer p, PreLoginEvent e){
		BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {
            try{
                PreparedStatement ps = Main.mysql.prepareStatement("INSERT INTO `PUNISHMENTS` (UUID,NAME) VALUES(?, ?) ON DUPLICATE KEY UPDATE NAME=?");
                ps.setString(1, p.getUniqueId().toString());
                ps.setString(2, p.getName());
                ps.setString(3, p.getName());
                Main.mysql.update(ps);
                PreparedStatement ps2 = Main.mysql.prepareStatement("SELECT * FROM `PUNISHMENTS` WHERE UUID=?");
                ps2.setString(1, p.getUniqueId().toString());
                ResultSet rs = Main.mysql.query(ps2);
                Boolean banned = false;
                Integer ban_expire = -1;
                String ban_reason = "UNKNOWN";
                String banned_by = "UNKNOWN";
                Boolean muted = false;
                Integer mute_expire = -1;
                String mute_reason = "UNKNOWN";
                String muted_by = "UNKNOWN";
                if (rs.next()){
                    ban_expire = rs.getInt("BANEXPIRE");
                    banned = !rs.wasNull();
                    ban_reason = rs.getString("BANREASON");
                    banned_by = rs.getString("BANBY");

                    mute_expire = rs.getInt("MUTEEXPIRE");
                    muted = !rs.wasNull();
                    mute_reason = rs.getString("MUTEREASON");
                    muted_by = rs.getString("MUTEBY");
                }
                PunishmentManagerOLD.muted.remove(p.getUniqueId());
                Boolean mute_expired = muted && mute_expire != -1 && mute_expire < Epoch.getEpoch();
                if (!mute_expired){
                    if (muted){
                        PunishmentManagerOLD.muted.add(p.getUniqueId());
                    }
                    PunishmentManagerOLD.mute_expire.put(p.getUniqueId(), mute_expire);
                    PunishmentManagerOLD.mute_reason.put(p.getUniqueId(), mute_reason);
                    PunishmentManagerOLD.muted_by.put(p.getUniqueId(), muted_by);
                }else{
                    unMute(p);
                }
                if (banned){
                    if (ban_expire == -1 || ban_expire > Epoch.getEpoch()){
                        String[] msg = {
                            "&8&m---------",
                            "&cBanned by: &e" + banned_by,
                            "&cReason: &e" + ban_reason,
                            "&cTime: " + timeFormat(ban_expire, "&e"), //[3]
                            "&c&lAPPEAL:&a http://tntwars.com/forums",
                            "&8&m---------"
                        };
                        if (ban_expire != -1){ //non-permanent ban
                            msg[3] = "&cTime: " + timeFormat(ban_expire - Epoch.getEpoch(),"&e") + "\n"
                                + "&cExpire: &6" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(ban_expire * 1000L));
                        }
                        Integer i = 0;
                        for (String s : msg){
                            msg[i] = ChatColor.translateAlternateColorCodes('&', s);
                            i++;
                        }
						BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {

                            //e.disallow(Result.KICK_BANNED, String.join("\n", msg));
							e.setCancelled(true);
							//noinspection deprecation
							p.disconnect(String.join("\n", msg));
                        });
                    }else if (ban_expire < Epoch.getEpoch()){
                        unBan(p);
                    }
                }
            }catch(Exception e1){
                e1.printStackTrace();
            }
        });
	}
	public static Boolean isMuted(ProxiedPlayer p){
		if (muted.contains(p.getUniqueId())){
			if (mute_expire.get(p.getUniqueId()) != -1 && mute_expire.get(p.getUniqueId()) < Epoch.getEpoch()){
				//Expired
				unMute(p);
				return false;
			}
			return true;
		}
		return false;
	}
	private static Integer getMuteExpire(ProxiedPlayer p){
		return mute_expire.get(p.getUniqueId());
	}
	private static String getMuteReason(ProxiedPlayer p){
		return mute_reason.get(p.getUniqueId());
	}
	private static String getMuter(ProxiedPlayer p){
		return muted_by.get(p.getUniqueId());
	}
	public static String timeFormat(int time, String color){
		if (time == -1){
			return "&c&lPERMANENT";
		}
        long days = TimeUnit.SECONDS
                .toDays(time);
        time -= TimeUnit.DAYS.toSeconds(days);

        long hours = TimeUnit.SECONDS
                .toHours(time);
        time -= TimeUnit.HOURS.toSeconds(hours);

        long minutes = TimeUnit.SECONDS
                .toMinutes(time);
        time -= TimeUnit.MINUTES.toSeconds(minutes);

        long seconds = TimeUnit.SECONDS
                .toSeconds(time);
        return color + timeFormat((int) days,(int) hours,(int) minutes,(int) seconds);
	}
	private static String timeFormat(int days, int hours, int minutes, int seconds) {
		String r = "";
		if (days != 0){
			if (days == 1){
				r += days + " day ";
			}else{
				r += days + " days ";
			}
		}
		if (hours != 0){
			if (hours == 1){
				r += hours + " hour ";
			}else{
				r += hours + " hours ";
			}
		}
		if (minutes != 0){
			if (minutes == 1){
				r += minutes + " minute ";
			}else{
				r += minutes + " minutes ";
			}
		}
		if (seconds != 0){
			if (seconds == 1){
				r += seconds + " second ";
			}else{
				r += seconds + " seconds ";
			}
		}
		r = removeLastChar(r);		
        return r;
    }
	
	private static String removeLastChar(String str) {
        return str.substring(0,str.length()-1);
    }

	static String replaceLast(String string, String substring, String replacement)
	{
	  int index = string.lastIndexOf(substring);
	  if (index == -1)
	    return string;
	  return string.substring(0, index) + replacement
	          + string.substring(index+substring.length());
	}
	public static void silentBan(UUID u, Integer time, String reason, String punisher, ProxiedPlayer p){
		try{
			PreparedStatement ps = Main.mysql.prepareStatement("UPDATE `PUNISHMENTS` SET BANEXPIRE=?, BANREASON=?, BANBY=? WHERE UUID=?");
			if (time < 0){
				ps.setInt(1, -1);
			}else{
				time = Epoch.getEpoch() + time;
				ps.setInt(1, time);
			}
			ps.setString(2, reason);
			ps.setString(3, punisher);
			ps.setString(4, u.toString());
			Main.mysql.update(ps);
			if (p == null){ return;}
			String[] msg = {
					"&8&m---------",
					"&cBanned by: &e" + punisher,
					"&cReason: &e" + reason,
					"&cTime: " + timeFormat(time, "&e"), //[3]
					"&c&lAPPEAL:&a http://tntwars.com/forums",
					"&8&m---------"
				};
			if (time != -1){ //non-permanent ban
				msg[3] = "&cTime: " + timeFormat(time - Epoch.getEpoch(),"&e") + "\n"
					+ "&cExpire: &6" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(time * 1000L));
			}
			Integer i =0;
			for (String s : msg){
				msg[i] = ChatColor.translateAlternateColorCodes('&', s);
				i++;
			}
			//noinspection deprecation
			p.disconnect(String.join("\n", msg));
		}catch(Exception e){
			System.out.println("Could not ban " + u.toString());
		}
	}

	public static void ban(UUID u, Integer time, String reason, ProxiedPlayer banner){
        BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {
            try{
                silentBan(u, time, reason, banner.getName(), banner);
                String name = PlayerUtil.getName(u.toString());
                String[] msg = {
                        bar,
                        "&4&lSTAFF VERSION",
                        "&cBanned: &e" + name,
                        "&cPunisher: &e" + banner.getName(),
                        "&cReason: &a" + reason,
                        "&cTime: &a" + timeFormat(time, ""),
                        bar
                };
                Integer i = 0;
                for (String s : msg){
                    msg[i] = ChatColor.translateAlternateColorCodes('&', s);
                    i++;
                }
                PlayerUtil.broadcastStaff(banner, String.join("\n", msg));
            }catch(Exception e){
                e.printStackTrace();
            }
        });


	}
	public static void banPublic(UUID u, Integer time, String reason, ProxiedPlayer banner){
        BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {
            try{
                silentBan(u, time, reason, banner.getName(), banner);
                String name = PlayerUtil.getName(u.toString());
                String[] msg = {
                        bar,
                        "&cBanned: &e" + name,
                        "&cTime: &a" + timeFormat(time, ""),
                        bar
                };
                Integer i = 0;
                for (String s : msg){
                    msg[i] = ChatColor.translateAlternateColorCodes('&', s);
                    i++;
                }
                PlayerUtil.broadcast(banner, String.join("\n", msg));
            }catch(Exception e){
                e.printStackTrace();
            }
        });


	}
	public static void silentMute(UUID u, Integer time, String reason, String punisher){
		try{
			PreparedStatement ps = Main.mysql.prepareStatement("UPDATE `PUNISHMENTS` SET MUTEEXPIRE=?, MUTEREASON=?, MUTEBY=? WHERE UUID=?");
			if (time < 0){
				ps.setInt(1, -1);
			}else{
				ps.setInt(1, Epoch.getEpoch() + time);
			}
			ps.setString(2, reason);
			ps.setString(3, punisher);
			ps.setString(4, u.toString());
			Main.mysql.update(ps);
		}catch(Exception e){
			System.out.println("Could not mute " + u.toString());
		}
	}
	public static void mutePublic(UUID u, Integer time, String reason, ProxiedPlayer banner){
        BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {
            try{
                silentMute(u, time, reason, banner.getName());
                String name = PlayerUtil.getName(u.toString());
                String[] msg = {
                        bar,
                        "&cMuted: &e" + name,
                        "&cTime: &a" + timeFormat(time, ""),
                        bar
                };
                Integer i = 0;
                for (String s : msg){
                    msg[i] = ChatColor.translateAlternateColorCodes('&', s);
                    i++;
                }
                PlayerUtil.broadcast(banner, String.join("\n", msg));
            }catch(Exception e){
                e.printStackTrace();
            }
        });
		
	}
	public static void mute(UUID u, Integer time, String reason, ProxiedPlayer banner){
        BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {
            try{
                silentMute(u, time, reason, banner.getName());
                String name = PlayerUtil.getName(u.toString());
                String[] msg = {
                        bar,
                        "&4&lSTAFF VERSION",
                        "&cMuted: &e" + name,
                        "&cPunisher: &e" + banner.getName(),
                        "&cReason: &a" + reason,
                        "&cTime: &a" + timeFormat(time, ""),
                        bar
                };
                Integer i = 0;
                for (String s : msg){
                    msg[i] = ChatColor.translateAlternateColorCodes('&', s);
                    i++;
                }
                PlayerUtil.broadcastStaff(banner, String.join("\n", msg));
            }catch(Exception e){
                e.printStackTrace();
            }
        });

	}
	public static void isMuted(String uuid, CallBackException<Void, Boolean> cb){
        BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {
            try{
                PreparedStatement ps = Main.mysql.prepareStatement("SELECT COUNT(*) as 'COUNT' FROM `PUNISHMENTS` WHERE `UUID`=? AND `MUTEEXPIRE` IS NOT NULL");
                ps.setString(1, uuid);
                ResultSet rs = Main.mysql.query(ps);
                Integer amount = -10;
                if (rs.next()){
                    amount = rs.getInt("COUNT");
                }
                cb.run(amount == 1);
            }catch(Exception e){
                cb.exception(e);
            }
        });
	}
	public static void isBanned(String uuid, CallBackException<Void, Boolean> cb){
        BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {
            try{
                PreparedStatement ps = Main.mysql.prepareStatement("SELECT COUNT(*) as 'COUNT' FROM `PUNISHMENTS` WHERE `UUID`=? AND `BANEXPIRE` IS NOT NULL");
                ps.setString(1, uuid);
                ResultSet rs = Main.mysql.query(ps);
                Integer amount = -10;
                if (rs.next()){
                    amount = rs.getInt("COUNT");
                }
                cb.run(amount == 1);
            }catch(Exception e){
                cb.exception(e);
            }
        });
	}
	public static void warnMute(ProxiedPlayer p){
		String[] msg = {
				PunishmentManagerOLD.bar,
				"&8&m---------",
				"&cMuted By: &e" + PunishmentManagerOLD.getMuter(p),
				"&cTime: &e" + PunishmentManagerOLD.timeFormat(PunishmentManagerOLD.getMuteExpire(p), "&e"),
				"&cReason: &e" + PunishmentManagerOLD.getMuteReason(p),
				"&c&lAPPEAL: &ahttp://tntwars.com/forums",
				"&8&m---------"
			};
			if (PunishmentManagerOLD.getMuteExpire(p) != -1){
				msg[2] = "&cTime: &e" + PunishmentManagerOLD.timeFormat(PunishmentManagerOLD.getMuteExpire(p) - Epoch.getEpoch(), "&e") +
					"\n" + "&cExpire: &6" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(PunishmentManagerOLD.getMuteExpire(p) * 1000L));
			}
			Integer i =0;
			for (String s : msg){
				msg[i] = ChatColor.translateAlternateColorCodes('&', s);
                //noinspection deprecation
                p.sendMessage(msg[i]);
				i++;
			}
	}
	public static void unMute(ProxiedPlayer p){
		muted.remove(p.getUniqueId());
		unMute(p.getUniqueId().toString());
	}
	public static void unMute(String uuid){

	}
	public static void unBan(ProxiedPlayer p){
		unBan(p.getUniqueId().toString());
	}
	public static void unBan(String uuid){
        BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {
				try{
					PreparedStatement ps = Main.mysql.prepareStatement("UPDATE `PUNISHMENTS` SET `BANEXPIRE`=? WHERE UUID=?");
					ps.setNull(1, java.sql.Types.INTEGER);
					ps.setString(2, uuid);
					Main.mysql.update(ps);
				}catch(Exception e){
					e.printStackTrace();
				}
			});
	}
}

