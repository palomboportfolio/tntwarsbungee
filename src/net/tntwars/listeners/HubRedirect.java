package net.tntwars.listeners;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.tntwars.Main;
import net.tntwars.MySQL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class HubRedirect implements Listener {
    private static List<ServerInfo> lobbies;

    public static void startUpdate() {
        BungeeCord.getInstance().getScheduler().schedule(Main.instance, () -> {
            List<ServerInfo> lobbies2;

            try {
                ProxyServer bungee = ProxyServer.getInstance();

                lobbies2 = new LinkedList<>();

                ResultSet r = MySQL.con.createStatement().executeQuery(
                        "SELECT * FROM DynamicServers");
                while (r.next()) {
                    String value = r.getString("TYPE");
                    if (value.equals("hub")) {
                        lobbies2.add(bungee.getServerInfo(r.getString(1)));
                    }
                }
                r.close();

                lobbies = lobbies2;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }, 1, 10, TimeUnit.SECONDS);
    }

    @EventHandler
    public void onJoin(ServerConnectEvent evt) {

        if (evt.getTarget().getName().equals("hub")) {
                Random rnd = new Random();
                int nextInt = rnd.nextInt(lobbies.size());
                ServerInfo newTarget = lobbies.get(nextInt);
                evt.setTarget(newTarget);
        }
    }
}