package net.tntwars.listeners;

import com.imaginarycode.minecraft.redisbungee.RedisBungee;
import com.imaginarycode.minecraft.redisbungee.RedisBungeeAPI;
import com.mysql.jdbc.PreparedStatement;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.tntwars.Main;
import net.tntwars.MySQL;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Ping implements Listener {

    public static int playerCount = 0;

    @EventHandler
    public void onPing(ProxyPingEvent evt) {
        try{
            Random rnd = new Random();
            int nextInt = rnd.nextInt(Main.motds.size());
            String motd = ChatColor.translateAlternateColorCodes((char)'&', Main.motds.get(nextInt));

            ServerPing sp = evt.getResponse();
            ServerPing.Players s = sp.getPlayers();
            s.setMax(s.getMax());
            sp.setDescriptionComponent(new TextComponent(TextComponent.fromLegacyText(motd)));
            s.setOnline(playerCount);
            setTotalCount(playerCount);
            sp.setPlayers(s);
            evt.setResponse(sp);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void startUpdate() {
        RedisBungeeAPI redis = RedisBungee.getApi();

        BungeeCord.getInstance().getScheduler().schedule(Main.instance, new Runnable() {
            public void run() {
                playerCount = redis.getPlayerCount();
            }

        }, 1, 10, TimeUnit.SECONDS);
    }
    
    public static void setTotalCount(int count) {
        BungeeCord.getInstance().getScheduler().runAsync(Main.instance, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement sql2 = (PreparedStatement) MySQL.con
                            .prepareStatement("SELECT COUNT FROM `DynamicServers` WHERE NAME=?;");
                    sql2.setString(1, "total");
                    PreparedStatement Save = (PreparedStatement) MySQL.con
                            .prepareStatement("UPDATE `DynamicServers` SET COUNT=? WHERE NAME=?;");
                    Save.setInt(1, count);
                    Save.setString(2, "total");
                    Save.executeUpdate();
                    Save.close();
                    sql2.close();
                } catch (Exception e) {
                    e.printStackTrace();
}
        }
        });
        }
        }
