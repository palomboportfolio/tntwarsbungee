package net.tntwars.listeners;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.tntwars.Main;
import net.tntwars.rank.Rank;
import net.tntwars.rank.RankType2;

import java.util.ArrayList;
import java.util.UUID;

public class PermissionsListener implements Listener {

    public static ArrayList<UUID> onlineStaff = new ArrayList<>();

    @EventHandler
    public void onJoin(PostLoginEvent event) {
        ProxiedPlayer player = event.getPlayer();

        BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {
            if (Rank.canDoStaff(player, RankType2.OWNER, false)) {
                for (String s : Main.ownerPermissions) {
                    player.setPermission(s, true);
                }
            }

            if (Rank.canDoStaff(player, RankType2.SYSADMIN, false)) {
                for (String s : Main.sysadminPermissions) {
                    player.setPermission(s, true);
                }
            }

            if (Rank.canDoStaff(player, RankType2.DEV, false)) {
                for (String s : Main.devPermissions) {
                    player.setPermission(s, true);
                }
            }

            if (Rank.canDoStaff(player, RankType2.ADMIN, false)) {
                for (String s : Main.adminPermissions) {
                    player.setPermission(s, true);
                }
            }

            if (Rank.canDoStaff(player, RankType2.MODPLUS, false)) {
                for (String s : Main.modplusPermissions) {
                    player.setPermission(s, true);
                }
            }

            if (Rank.canDoStaff(player, RankType2.MOD, false)) {
                for (String s : Main.modPermissions) {
                    player.setPermission(s, true);
                }
            }

            if (Rank.canDoStaff(player, RankType2.HELPER, false)) {
                for (String s : Main.helperPermissions) {
                    player.setPermission(s, true);
                }
            }

            for (String s : Main.defautPermissions) {
                player.setPermission(s, true);
            }
        });
    }
}
