package net.tntwars.listeners;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.tntwars.rank.Rank;
import net.tntwars.rank.RankType2;

public class PermissionCheck implements Listener {
    @EventHandler
    public void onPermissionCheck(PermissionCheckEvent event) {
        if (event.getSender() instanceof ProxiedPlayer) {
            try {
                if (Rank.canDoStaff((ProxiedPlayer) event.getSender(), RankType2.DEV, false)) {
                    event.setHasPermission(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
