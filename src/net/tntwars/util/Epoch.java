package net.tntwars.util;

public class Epoch {
	public static int getEpoch(){
		return Math.round(System.currentTimeMillis() / 1000);
	}
}
