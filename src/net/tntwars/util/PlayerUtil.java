package net.tntwars.util;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.tntwars.CallBackException;
import net.tntwars.Main;
import net.tntwars.PlayerNotFoundException;

import java.io.IOException;
import java.util.UUID;

public class PlayerUtil{
	public static void getUUID(String name, CallBackException<Void, UUID> cb) {
		BungeeCord.getInstance().getScheduler().runAsync(Main.instance, () -> {
			try {
				String uuid = HTTP.getText("http://tntwars.net/uuid.php?NAME=" + name);
				cb.run(UUID.fromString(insertDashUUID(uuid)));
			} catch (IOException e) {
				cb.exception(new PlayerNotFoundException());
			}
		});
	}

	private static String insertDashUUID(String uuid) {
		StringBuffer sb = new StringBuffer(uuid);
		sb.insert(8, "-");

		sb = new StringBuffer(sb.toString());
		sb.insert(13, "-");

		sb = new StringBuffer(sb.toString());
		sb.insert(18, "-");

		sb = new StringBuffer(sb.toString());
		sb.insert(23, "-");

		return sb.toString();
	}

	public static String getName(String uuid) throws IOException{
		return HTTP.getText("http://tntwars.net/name.php?UUID=" + uuid);
	}

	public static void broadcast(ProxiedPlayer bcaster, String msg){
		BungeeCord.getInstance().broadcast(bcaster.getDisplayName() + msg);
	}

	public static void broadcastStaff(ProxiedPlayer bcaster, String msg){
		BungeeCord.getInstance().broadcast(bcaster.getDisplayName() + msg);
	}

}
