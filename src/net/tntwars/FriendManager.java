package net.tntwars;

import com.mysql.jdbc.PreparedStatement;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class FriendManager implements Listener {

    public static HashMap<UUID, ArrayList<UUID>> friendRequest = new HashMap<>();
    public static HashMap<UUID, ArrayList<UUID>> onlineFriends = new HashMap<>();
    public static HashMap<UUID, ArrayList<UUID>> friends = new HashMap<>();
    public static HashMap<UUID, ArrayList<UUID>> blockedPlayers = new HashMap<>();

    @EventHandler
    public void onJoin(PostLoginEvent event) {
        ProxiedPlayer player = event.getPlayer();
        ArrayList<UUID> array = new ArrayList<>();

        array.add(player.getUniqueId());

        friends.put(player.getUniqueId(), array);
        onlineFriends.put(player.getUniqueId(), array);
        friendRequest.put(player.getUniqueId(), array);
        blockedPlayers.put(player.getUniqueId(), array);
    }

    public static boolean inDatabase(UUID uuid) {
        try {
            PreparedStatement sql = (PreparedStatement) MySQL.con.prepareStatement("SELECT * FROM `FRIENDS` WHERE sender=?;");
            sql.setString(1, uuid.toString());

            ResultSet resultSet = sql.executeQuery();
            boolean contains = resultSet.next();

            sql.close();
            resultSet.close();

            return !contains;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void loadPlayer(UUID uuid) {
                try {
                    ResultSet r = MySQL.con.createStatement().executeQuery(
                            "SELECT * FROM FRIENDS WHERE sender = '" + uuid.toString() + "'");

                    while (r.next()) {
                        if (r.getString("status").equals("friends")) {
                            ArrayList<UUID> array = new ArrayList<>();

                            array.add(UUID.fromString(r.getString(1)));
                            friends.put(uuid, array);
                        } else if (r.getString("status").equals("pending")) {
                            ArrayList<UUID> array = new ArrayList<>();

                            array.add(UUID.fromString(r.getString(1)));
                            friendRequest.put(uuid, array);
                        } else if (r.getString("status").equals("blocked")) {
                            ArrayList<UUID> array = new ArrayList<>();

                            array.add(UUID.fromString(r.getString(1)));
                            blockedPlayers.put(uuid, array);
                        }
                    }
                    r.close();
                } catch(Exception e) {
                    e.printStackTrace();
                }
    }

    public static void addFriend(UUID uuid, UUID friend) {
        try {
            PreparedStatement newFriend = (PreparedStatement) MySQL.con
                    .prepareStatement("INSERT INTO `FRIENDS` values(?,?);");
            newFriend.setString(1, uuid.toString());
            newFriend.setString(2, friend.toString());
            newFriend.execute();

            PreparedStatement newFriend1 = (PreparedStatement) MySQL.con
                    .prepareStatement("INSERT INTO `FRIENDS` values(?,?);");
            newFriend1.setString(1, friend.toString());
            newFriend1.setString(2, uuid.toString());
            newFriend1.execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeFriend(UUID uuid, UUID friend) {
        try {
            ResultSet r = MySQL.con.createStatement().executeQuery(
                    "SELECT * FROM FRIENDS WHERE sender = '" + uuid.toString() + "'");

            while (r.next()) {
                if (r.getString(1).equals(uuid.toString()) && r.getString(2).equals(friend.toString())) {
                    r.deleteRow();
                }

                if (r.getString(1).equals(friend.toString()) && r.getString(2).equals(uuid.toString())) {
                    r.deleteRow();
                }
            }
            r.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void denyFriend(UUID uuid, UUID friend) {
        try {
            ResultSet r = MySQL.con.createStatement().executeQuery(
                    "SELECT * FROM FRIENDS WHERE sender = '" + uuid.toString() + "'");

            while (r.next()) {
                if (r.getString(1).equals(uuid.toString()) && r.getString(2).equals(friend.toString())) {

                }

                if (r.getString(1).equals(friend.toString()) && r.getString(2).equals(uuid.toString())) {
                    r.deleteRow();
                }
            }
            r.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isFriend(UUID uuid, UUID friend) {
        try {
            ResultSet r = MySQL.con.createStatement().executeQuery(
                    "SELECT * FROM FRIENDS WHERE sender = '" + uuid.toString() + "'");

            while (r.next()) {
                if (r.getString(2).contains(friend.toString())) {
                    return true;
                }
            }
            r.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void sendFriendRequest(UUID uuid, UUID friend) {
        try {
            PreparedStatement newFriend = (PreparedStatement) MySQL.con
                    .prepareStatement("INSERT INTO `FRIENDS_REQUEST` values(?,?);");
            newFriend.setString(1, uuid.toString());
            newFriend.setString(2, friend.toString());
            newFriend.execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean hasFriendRequest(UUID uuid, UUID friend) {
        try {
            ResultSet r = MySQL.con.createStatement().executeQuery(
                    "SELECT * FROM FRIENDS_REQUEST WHERE sender = '" + uuid.toString() + "'");

            while (r.next()) {
                if (r.getString(2).contains(friend.toString())) {
                        return true;
                }
            }
            r.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static ArrayList<UUID> getFriends(UUID uuid) {
        try {
            ResultSet r = MySQL.con.createStatement().executeQuery(
                    "SELECT * FROM FRIENDS WHERE sender = '" + uuid.toString() + "'");

            friends.get(uuid).clear();

            while (r.next()) {
                ArrayList<UUID> array = new ArrayList<>();

                array.add(UUID.fromString(r.getString(2)));
                friends.put(uuid, array);
            }
            r.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return friends.get(uuid);
    }

    public static ArrayList<UUID> getOnlineFriends(UUID uuid) {
        return onlineFriends.get(uuid);
    }
}
