package net.tntwars;

import com.imaginarycode.minecraft.redisbungee.RedisBungee;
import fr.Alphart.BAT.BAT;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.tntwars.commands.*;
import net.tntwars.commands.report.RedisReport;
import net.tntwars.commands.report.Report;
import net.tntwars.listeners.HubRedirect;
import net.tntwars.listeners.PermissionCheck;
import net.tntwars.listeners.PermissionsListener;
import net.tntwars.listeners.Ping;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Main extends Plugin implements Listener {

    public static Main instance;
    public static MySQL mysql;
    public String host;
    public Integer port;
    public String database;
    public String username;
    public String password;
    public static String lastReset;
    public static List<String> disabledservers = new ArrayList();
    public static ConfigurationProvider cp = ConfigurationProvider.getProvider(YamlConfiguration.class);
    public static HashMap<UUID, Integer> playertimes = new HashMap();

    public static ArrayList<String> ownerPermissions = new ArrayList<>();
    public static ArrayList<String> sysadminPermissions = new ArrayList<>();
    public static ArrayList<String> devPermissions = new ArrayList<>();
    public static ArrayList<String> adminPermissions = new ArrayList<>();
    public static ArrayList<String> modplusPermissions = new ArrayList<>();
    public static ArrayList<String> modPermissions = new ArrayList<>();
    public static ArrayList<String> helperPermissions = new ArrayList<>();
    public static ArrayList<String> defautPermissions = new ArrayList<>();

    public static ArrayList<String> motds = new ArrayList<>();

    public void onEnable() {
        instance = this;

        //Listeners
        getProxy().getPluginManager().registerListener(this, new HubRedirect());
        getProxy().getPluginManager().registerListener(this, new Ping());
        getProxy().getPluginManager().registerListener(this, new PermissionsListener());
        getProxy().getPluginManager().registerListener(this, new PermissionCheck());
        getProxy().getPluginManager().registerListener(this, new FriendManager());
        //getProxy().getPluginManager().registerListener(this, new PunishmentManager());

        //Commands
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new Report("report"));
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new RedisReport("redisreport"));
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new RedisKick("rediskick"));
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new Warn("warn"));
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new Talert("talert"));
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new Reload("reload"));
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new Hub("hub"));
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new FriendCMD.Friend("friend"));
        //BungeeCord.getInstance().getPluginManager().registerCommand(this, new Punish.Ban("ban"));
        //BungeeCord.getInstance().getPluginManager().registerCommand(this, new Punish.TempBan("tempban"));
        //BungeeCord.getInstance().getPluginManager().registerCommand(this, new Punish.Mute("mute"));
        //BungeeCord.getInstance().getPluginManager().registerCommand(this, new Punish.TempMute("tempmute"));
        //BungeeCord.getInstance().getPluginManager().registerCommand(this, new Punish.Kick("kick"));
        //BungeeCord.getInstance().getPluginManager().registerCommand(this, new Punish.UnBan("unban"));
        //BungeeCord.getInstance().getPluginManager().registerCommand(this, new Punish.UnMute("unmute"));
        //BungeeCord.getInstance().getPluginManager().registerCommand(this, new Punish.ClearHistory("ch"));


        File dir = new File(String.valueOf(getDataFolder()));

        if (!dir.exists()) {
            dir.mkdir();
        }

        File file = new File(getDataFolder(), "config.yml");

        if (!(file.exists())) {
            createConfig();
        } else {
            loadConfig();
        }

        connectMySQL();

        HubRedirect.startUpdate();
        Ping.startUpdate();
    }
    public static boolean isPlayerOnline(ProxiedPlayer player) {
        return (BAT.getInstance().getRedis().isRedisEnabled()) && (RedisBungee.getApi().isPlayerOnline(player.getUniqueId()));
    }

    public void createConfig()
    {
        try
        {
            File file = new File(getDataFolder(), "config.yml");

            if (!file.exists())
            {
                file.createNewFile();
                Configuration cfg = cp.load(file);

                cfg.set("permissions.owner", "");
                cfg.set("permissions.sysadmin", "");
                cfg.set("permissions.dev", "");
                cfg.set("permissions.admin", "");
                cfg.set("permissions.modplus", "");
                cfg.set("permissions.mod", "");
                cfg.set("permissions.helper", "");
                cfg.set("permissions.default", "");

                cfg.set("MySQL.host", "localhost");
                cfg.set("MySQL.database", "database");
                cfg.set("MySQL.username", "username");
                cfg.set("MySQL.password", "password");

                cfg.set("MOTD", "");

                cp.save(cfg, file);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void loadConfig()
    {
        try
        {
            File file = new File(getDataFolder(), "config.yml");
            Configuration cfg = cp.load(file);

            this.host = cfg.getString("MySQL.host");
            this.database = cfg.getString("MySQL.database");
            this.username = cfg.getString("MySQL.username");
            this.password = cfg.getString("MySQL.password");

            this.ownerPermissions = (ArrayList<String>) cfg.getStringList("permissions.owner");
            this.sysadminPermissions = (ArrayList<String>) cfg.getStringList("permissions.sysadmin");
            this.devPermissions = (ArrayList<String>) cfg.getStringList("permissions.dev");
            this.adminPermissions = (ArrayList<String>) cfg.getStringList("permissions.admin");
            this.modplusPermissions = (ArrayList<String>) cfg.getStringList("permissions.modplus");
            this.modPermissions = (ArrayList<String>) cfg.getStringList("permissions.mod");
            this.helperPermissions = (ArrayList<String>) cfg.getStringList("permissions.helper");
            this.defautPermissions = (ArrayList<String>) cfg.getStringList("permissions.default");

            this.motds = (ArrayList<String>) cfg.getStringList("MOTD");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void reloadConfig() {
        File file = new File(instance.getDataFolder(), "config.yml");
        try {
            Configuration cfg = cp.load(file);

            instance.host = cfg.getString("MySQL.host");
            instance.database = cfg.getString("MySQL.database");
            instance.username = cfg.getString("MySQL.username");
            instance.password = cfg.getString("MySQL.password");

            instance.ownerPermissions = (ArrayList<String>) cfg.getStringList("permissions.owner");
            instance.sysadminPermissions = (ArrayList<String>) cfg.getStringList("permissions.sysadmin");
            instance.devPermissions = (ArrayList<String>) cfg.getStringList("permissions.dev");
            instance.adminPermissions = (ArrayList<String>) cfg.getStringList("permissions.admin");
            instance.modplusPermissions = (ArrayList<String>) cfg.getStringList("permissions.modplus");
            instance.modPermissions = (ArrayList<String>) cfg.getStringList("permissions.mod");
            instance.helperPermissions = (ArrayList<String>) cfg.getStringList("permissions.helper");
            instance.defautPermissions = (ArrayList<String>) cfg.getStringList("permissions.default");

            instance.motds = (ArrayList<String>) cfg.getStringList("MOTD");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void connectMySQL() {

        File file = new File(getDataFolder(), "config.yml");
        Configuration cfg = null;
        try {
            cfg = cp.load(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String ip = cfg.getString("MySQL.host");
        String name = cfg.getString("MySQL.database");
        String username = cfg.getString("MySQL.username");
        String password = cfg.getString("MySQL.password");

        mysql = new MySQL(ip, name, username, password);
                if (!mysql.hasConnection()){
                    mysql.connect();
                }

    }

    public static String timeFormat(int days, int hours, int minutes, int seconds) {
        String r = "";
        if (days != 0){
            if (days == 1){
                r += days + " day ";
            }else{
                r += days + " days ";
            }
        }
        if (hours != 0){
            if (hours == 1){
                r += hours + " hour ";
            }else{
                r += hours + " hours ";
            }
        }
        if (minutes != 0){
            if (minutes == 1){
                r += minutes + " minute ";
            }else{
                r += minutes + " minutes ";
            }
        }
        if (seconds != 0){
            if (seconds == 1){
                r += seconds + " second ";
            }else{
                r += seconds + " seconds ";
            }
        }
        r = removeLastChar(r);
        return r;
    }

    private static String removeLastChar(String str) {
        return str.substring(0,str.length()-1);
    }


    public static String color(String s){
        return ChatColor.translateAlternateColorCodes('&', s);
    }
}