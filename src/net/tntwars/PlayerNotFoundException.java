package net.tntwars;

public class PlayerNotFoundException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    //Parameterless Constructor
    public PlayerNotFoundException() {}

    //Constructor that accepts a message
    public PlayerNotFoundException(String message)
    {
        super(message);
    }

}
