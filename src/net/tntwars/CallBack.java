package net.tntwars;

public interface CallBack<R, A> {

    public R run(A arg);

}