package net.tntwars.commands;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.tntwars.rank.Rank;

import java.util.HashMap;
import java.util.UUID;

public class Warn
extends Command {
    private HashMap<UUID, Long> time = new HashMap<>();
    private int coolDown = 12000;

    public Warn(String name) {
        super(name);
    }
    
	public void execute(CommandSender cs, String[] args) {
        if (cs instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer)cs;
            if(!Rank.isStaff(p)) {
                p.sendMessage(new TextComponent("§c§lWARN §8§l\u00bb §7You don't have enough Permissions!"));
                return;
            }
            if (args.length >= 2) {
                ProxiedPlayer t = BungeeCord.getInstance().getPlayer(args[0]);
                if (t != null && t.isConnected()) {
                    if (this.canReport(p.getUniqueId())) {
                        String type;
                        String punishment;
                        if (args[1].equalsIgnoreCase("hacker") || args[1].equalsIgnoreCase("hack") || args[1].equalsIgnoreCase("hacking")) {
                            type = "Hacking";
                            punishment = "Hacking will result in a §cpermanant ban§7! If you are §7hacking, log off, remove your hacked client, and rejoin, §7or §7further action will be taken!";
                        } else if (args[1].equalsIgnoreCase("grief") || args[1].equalsIgnoreCase("griefer") || args[1].equalsIgnoreCase("griefing")) {
                            type = "Griefing";
                            punishment = "Griefing will result in a §c7 day ban§7! If you are §7purposefully attacking or otherwise sabotaging your §7teammates, §cSTOP NOW§7, or further action will be taken!";
                        } else if (args[1].equalsIgnoreCase("chat") || args[1].equalsIgnoreCase("chat-abuse") || args[1].equalsIgnoreCase("language") || args[1].equalsIgnoreCase("lang")) {
                            type = "Language";
                            punishment = "Strong or hateful language will result in being §cmuted or possibly banned§7! Please refrain from using such §clanguage or further action will be taken!";
                        } else if (args[1].equalsIgnoreCase("other")) {
                        	if(!(args.length >= 3)){
                        		p.sendMessage(new TextComponent("§c§lREPORT §8§l\u00bb §7In order to use type: other, §7you must add a message to send to the player!"));
                        		p.sendMessage(new TextComponent("§c§lWARN §8§l\u00bb §7/warn <player> other <explanation>"));
                        		return;
                        	}
                            type = "Other";
                            String punish = "";
                            for(int i = 2; i <= args.length - 1; i++){
                            	punish += (args[i] + " ");
                            } 
                            punishment = punish;
                        } else {
                            p.sendMessage(new TextComponent("§c§lWARN §8§l\u00bb §7Invalid type!"));
                            p.sendMessage(new TextComponent("§c§lWARN §8§l\u00bb §7Available Types: Hacking, Griefing, Language, §7Other"));
                            return;
                        }
                        t.sendMessage(new TextComponent("§8§m-----------------------------------------------------"));
                        t.sendMessage(new TextComponent("§c§lWARNING: §8§l\u00bb §7 You have been warned by: §c" + p.getName() + " §7For §7reason: §c" + type));
                        t.sendMessage(new TextComponent("§8§l\u00bb §7" + punishment));
                        t.sendMessage(new TextComponent("§8§m-----------------------------------------------------"));
                        
                        if (!type.isEmpty()) {
                            this.time.put(p.getUniqueId(), System.currentTimeMillis());
                            String server = t.getServer().getInfo().getName();
                            p.sendMessage(new TextComponent("§c§lWARN §8§l\u00bb §cYour warning was sent!"));
                            for (ProxiedPlayer pl : BungeeCord.getInstance().getPlayers()) {
                                if (!pl.hasPermission("report.staff.see")) continue;
                                pl.sendMessage(new TextComponent("§8§m-----------------------------------------------------"));
                                pl.sendMessage(new TextComponent("§8\u00bb §7Warned by: §6" + p.getName()));
                                pl.sendMessage(new TextComponent("§8\u00bb §7Offender: §c" + t.getName()));
                                pl.sendMessage(new TextComponent("§8\u00bb §7Server: §c" + server));
                                pl.sendMessage(new TextComponent("§8\u00bb §7Type: §c" + type));
                                pl.sendMessage(new TextComponent(""));
                                TextComponent st = new TextComponent("§c§l[CLICK TO INVESTIGATE]");
                                st.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§fClick to connect to §e" + server + "§f.").create()));
                                st.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/server " + server));
                                pl.sendMessage(st);
                                pl.sendMessage(new TextComponent("§8§m-----------------------------------------------------"));
                            }
                        } else {
                            p.sendMessage(new TextComponent("§c§lWARN §8§l\u00bb §7Invalid type!"));
                            p.sendMessage(new TextComponent("§c§lWARN §8§l\u00bb §7Available Types: Hacking, Griefing, Language, §7Other"));
                        }
                    } else {
                        double timeleft = Math.round((this.time.get(p.getUniqueId()) + (long)this.coolDown - System.currentTimeMillis()) / 1000 * 100);
                        p.sendMessage(new TextComponent("§c§lWARN §8§l\u00bb §7You can warn a player again in §e" + String.valueOf(timeleft / 100.0) + " §7seconds!"));
                    }
                } else {
                    p.sendMessage(new TextComponent("§c§lWARN §8§l\u00bb §7You cannot warn §e" + args[0] + "§7."));
                }
            } else {
                p.sendMessage(new TextComponent("§c§lWARN §8§l\u00bb §7/warn <player> <type>"));
            }
        }
    }

    private boolean canReport(UUID uuid) {
        if (this.time.containsKey(uuid)) {
            long current = System.currentTimeMillis();
            return this.time.get(uuid) + (long) this.coolDown <= current;
        }
        return true;
    }
}

