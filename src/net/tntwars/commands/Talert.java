package net.tntwars.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.regex.Pattern;

public class Talert extends Command {

    public Talert(String name) {
        super(name);
    }

    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            //noinspection deprecation
            sender.sendMessage(ChatColor.RED + "You must add a message!");
        } else {
            StringBuilder builder = new StringBuilder();
            int j = args.length;
            int i = 0;
            while (i < j) {
                String s;
                s = args[i];
                builder.append(ChatColor.translateAlternateColorCodes('&', s));
                builder.append(" ");
                ++i;
            }
            String message = builder.substring(0, builder.length() - 1);
            String[] parts = message.split(Pattern.quote("|"));
            Title title = ProxyServer.getInstance().createTitle();
            TextComponent titleComp = new TextComponent("§c§l" + parts[0]);
            titleComp.setBold(Boolean.TRUE);
            for (ProxiedPlayer p1 : ProxyServer.getInstance().getPlayers()) {
                p1.sendMessage(new TextComponent("§c§l" + parts[0] + "§7§l\u00bb" + parts[1]));
            }
            TextComponent titleSubComp = new TextComponent(parts[1]);
            title.title(titleComp);
            title.subTitle(titleSubComp);
            title.fadeIn(40);
            title.stay(100);
            title.fadeOut(40);
            for (ProxiedPlayer proxplayer : ProxyServer.getInstance().getPlayers()) {
                proxplayer.sendTitle(title);
            }
        }
    }
}