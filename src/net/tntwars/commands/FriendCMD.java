package net.tntwars.commands;

import com.imaginarycode.minecraft.redisbungee.RedisBungee;
import com.imaginarycode.minecraft.redisbungee.RedisBungeeAPI;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.tntwars.FriendManager;

import java.util.UUID;

public class FriendCMD {

    public static class Friend extends Command {
        public Friend(String name) {
            super(name);
        }

        public void execute(CommandSender sender, String[] args) {
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) sender;
                RedisBungeeAPI redis = RedisBungee.getApi();

                if (args.length == 0) {
                    usageMessage(player);
                } else if (args[0].equalsIgnoreCase("list")) {
                    if (FriendManager.getFriends(player.getUniqueId()).size() > 0) {
                        player.sendMessage("§8§l§m--------------------------");
                        player.sendMessage("§7Your Friends List:");
                        player.sendMessage("  §7" + FriendManager.getFriends(player.getUniqueId()));
                        player.sendMessage("§8§l§m--------------------------");
                    } else {
                        player.sendMessage("§5§lFriends §7You currently do not have any friends.");
                    }
                } else if (args[0].equalsIgnoreCase("test")) {
                    player.sendMessage("--");
                    player.sendMessage(FriendManager.getFriends(player.getUniqueId()).toString());
                    player.sendMessage("  ");
                    player.sendMessage("--");
                } else if (args.length > 1) {

                    player.sendMessage(redis.getUuidFromName(args[1]) + "");
                    player.sendMessage(args[1]);

                    if (redis.isPlayerOnline(redis.getUuidFromName(args[1]))) {

                        String targetName = args[1];
                        UUID targetUUID = UUID.fromString(redis.getUuidFromName(args[1]).toString());


                            if (args[0].equalsIgnoreCase("add")) {
                                if (FriendManager.isFriend(player.getUniqueId(), targetUUID)) {
                                    player.sendMessage("§5§lFriends §cYou are already friends with this player.");
                                } else {
                                    if (FriendManager.getFriends(player.getUniqueId()).size() != 10) {
                                        if ((FriendManager.hasFriendRequest(player.getUniqueId(), targetUUID))) {
                                            player.sendMessage("§5§lFriends §cYou already have a pending request to this person.");
                                        } else {
                                            FriendManager.sendFriendRequest(player.getUniqueId(), targetUUID);
                                            player.sendMessage("§5§lFriends §aRequest sent!");
                                        }
                                    } else {
                                        player.sendMessage("§5§lFriends §cYou have reached the max amount of friends (10).");
                                    }
                                }
                            } else if (args[0].equalsIgnoreCase("remove")) {
                                if (FriendManager.isFriend(player.getUniqueId(), targetUUID)) {
                                    FriendManager.removeFriend(player.getUniqueId(), targetUUID);
                                    player.sendMessage("§5§lFriends §4You have removed §4§l" + targetName + "§4.");
                                } else {
                                    player.sendMessage("§5§lFriends §cThis player is not your friend.");
                                }
                            } else if (args[0].equalsIgnoreCase("accept")) {
                                if (FriendManager.isFriend(player.getUniqueId(), targetUUID)) {
                                    player.sendMessage("§5§lFriends §cYou are already friends with this player.");
                                } else {
                                    if (FriendManager.getFriends(player.getUniqueId()).size() == 10) {
                                        player.sendMessage("§5§lFriends §cYou may not accept any request, you have too many friends.");
                                    } else {
                                        if (FriendManager.hasFriendRequest(targetUUID, player.getUniqueId())) {
                                            FriendManager.addFriend(player.getUniqueId(), targetUUID);
                                            player.sendMessage("§5§lFriends §aYou have accept §a§l" + targetName + "§a's request.");
                                        } else {
                                            player.sendMessage("§5§lFriends §cYou do not have a pending request from this player.");
                                        }
                                    }
                                }
                            } else if (args[0].equalsIgnoreCase("deny")) {
                                if (FriendManager.hasFriendRequest(player.getUniqueId(), targetUUID)) {
                                    player.sendMessage("§5§lFriends §4You have denied §a§l" + targetName + "§a's request.");
                                    FriendManager.denyFriend(player.getUniqueId(), targetUUID);
                                } else {
                                    player.sendMessage("§5§lFriends §cYou do not have a pending request from this player.");
                                }
                            } else if (args[0].equalsIgnoreCase("join")) {
                                player.sendMessage("To Do");
                            } else {
                                usageMessage(player);
                            }
                    } else {
                        player.sendMessage("§5§lFriends §cThis player is not online.");
                    }
                } else {
                    usageMessage(player);
                }
            } else {
                sender.sendMessage("§c§lYou must be a player to use this command.");
            }
        }
    }

    public static void usageMessage(ProxiedPlayer player) {
        player.sendMessage("§8§l§m--------------------------");
        player.sendMessage("  §7/friend list");
        player.sendMessage("  §7/friend add [player]");
        player.sendMessage("  §7/friend remove [player]");
        player.sendMessage("  §7/friend accept [player]");
        player.sendMessage("  §7/friend deny [player]");
        player.sendMessage("  §7/friend join [player]");
        player.sendMessage("§8§l§m--------------------------");
    }
}
