package net.tntwars.commands;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;
import net.tntwars.Main;
import net.tntwars.listeners.PermissionsListener;

import java.util.UUID;

public class Notify extends Command {

    public Notify(String name) {
        super(name);
    }

    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(new TextComponent(ChatColor.RED + "You must add a message!"));
            return;
        }

        StringBuilder strBuilder = new StringBuilder();
        for (int i = 1; i <= args.length; i++) {
            strBuilder.append(args[i]);
        }
        String message = strBuilder.toString();

        switch(args[0]){
            case "all":{
                BungeeCord.getInstance().broadcast(new TextComponent(Main.color(message)));
                return;
            }

            case "staff":{
                for (UUID staffMember : PermissionsListener.onlineStaff) {
                    if(!BungeeCord.getInstance().getPlayer(staffMember).isConnected()){
                        PermissionsListener.onlineStaff.remove(staffMember);
                        return;
                    }
                    BungeeCord.getInstance().getPlayer(staffMember).sendMessage(new TextComponent(Main.color(message)));
                }
                return;
            }
            default:{
                BungeeCord.getInstance().getPlayer(args[0]).sendMessage(new TextComponent(Main.color(message)));
            }
        }
    }
}