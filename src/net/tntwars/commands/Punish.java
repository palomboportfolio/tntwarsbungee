package net.tntwars.commands;

import com.imaginarycode.minecraft.redisbungee.RedisBungee;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.tntwars.Main;
import net.tntwars.PunishmentManager;
import net.tntwars.rank.Rank;
import net.tntwars.rank.RankType2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Punish {
    final static String no_permission = Main.color("&7You must be a &cStaff member &7to use this command!");

    public static class Ban extends Command {
        public Ban(String name) {super(name);}
        final private String usage = Main.color("&a/ban <player> <reason> &3Moderator rank or above");

        public void execute(CommandSender cs, String[] args) {
			if (!Rank.canDoStaff((ProxiedPlayer) cs, RankType2.MOD, false)) {
				cs.sendMessage(new TextComponent(Punish.no_permission));
				return;
			}


			if (args.length < 1) {
                cs.sendMessage(new TextComponent(usage));
                return;
            }
            ArrayList<String> al = new ArrayList<>(Arrays.asList(args));
            String reason = String.join(" ", al.remove(0));

            if (!(cs instanceof ProxiedPlayer)) {PunishmentManager.punish(RedisBungee.getApi().getUuidFromName(args[0]), "ban", -1, "CONSOLE", reason);}
            else {PunishmentManager.punish(RedisBungee.getApi().getUuidFromName(args[0]), "ban", -1, cs.getName(), reason);}
        }
    }

    public static class TempBan extends Command {
        public TempBan(String name) {super(name);}
        final private String usage = Main.color("&a/tempban <player> <time> <timeunit> <reason> &3Moderator rank or above");

        public void execute(CommandSender cs, String[] args) {
			if (!Rank.canDoStaff((ProxiedPlayer) cs, RankType2.MOD, false)) {
				cs.sendMessage(new TextComponent(Punish.no_permission));
				return;
			}

			if (args.length < 4) {
                cs.sendMessage(new TextComponent(usage));
                return;
            }
            ArrayList<String> al = new ArrayList<>(Arrays.asList(args));
            for (Integer i = 0; i < 3; i++) {al.remove(1);}
            String reason = String.join(" ", al);
            Integer seconds = Punish.getSeconds(args[1], args[2]);
            ArrayList<String> valid = new ArrayList<>();
            for (Punish.Time t2 : Punish.Time.values()) {Collections.addAll(valid, t2.getMatches());}
            if (seconds == 0) {cs.sendMessage(new TextComponent(usage + " &aInvalid <time>"));}
            else if (seconds == -1) {cs.sendMessage(new TextComponent(Main.color("&cInvalid time unit, valid: &a" + String.join(", ", valid))));}

            if (!(cs instanceof ProxiedPlayer)) {PunishmentManager.punish(RedisBungee.getApi().getUuidFromName(args[0]), "tempban", seconds, "CONSOLE", reason);}
            else {PunishmentManager.punish(RedisBungee.getApi().getUuidFromName(args[0]), "tempban", seconds, cs.getName(), reason);}
        }
    }

    public static class Mute extends Command {
        public Mute(String name) {super(name);}
        final private String usage = Main.color("&a/mute <player> <reason> &9Helper rank or above");

        public void execute(CommandSender cs, String[] args) {
			if (!Rank.canDoStaff((ProxiedPlayer) cs, RankType2.HELPER, false)) {
				cs.sendMessage(new TextComponent(Punish.no_permission));
				return;
			}


			if (args.length < 1) {
                cs.sendMessage(new TextComponent(usage));
                return;
            }
            ArrayList<String> al = new ArrayList<>(Arrays.asList(args));
            String reason = String.join(" ", al.remove(0));

            if (!(cs instanceof ProxiedPlayer)) {PunishmentManager.punish(RedisBungee.getApi().getUuidFromName(args[0]), "mute", -1, "CONSOLE", reason);}
            else {PunishmentManager.punish(RedisBungee.getApi().getUuidFromName(args[0]), "mute", -1, cs.getName(), reason);}
        }
    }

    public static class TempMute extends Command {
        public TempMute(String name) {super(name);}
        final private String usage = Main.color("&a/tempmute <player> <time> <timeunit> <reason> &9Helper rank or above");



        public void execute(CommandSender cs, String[] args) {
			if (!Rank.canDoStaff((ProxiedPlayer) cs, RankType2.HELPER, false)) {
				cs.sendMessage(new TextComponent(Punish.no_permission));
				return;
			}


			if (args.length < 4) {
                cs.sendMessage(new TextComponent(usage));
                return;
            }
            ArrayList<String> al = new ArrayList<>(Arrays.asList(args));
            for (Integer i = 0; i < 3; i++) {al.remove(1);}
            String reason = String.join(" ", al);
            Integer seconds = Punish.getSeconds(args[1], args[2]);
            ArrayList<String> valid = new ArrayList<>();
            for (Punish.Time t2 : Punish.Time.values()) {Collections.addAll(valid, t2.getMatches());}
            if (seconds == 0) {cs.sendMessage(new TextComponent(usage + " &aInvalid <time>"));}
            else if (seconds == -1) {cs.sendMessage(new TextComponent(Main.color("&cInvalid time unit, valid: &a" + String.join(", ", valid))));}

            if (!(cs instanceof ProxiedPlayer)) {
				PunishmentManager.punish(RedisBungee.getApi().getUuidFromName(args[0]), "tempmute", seconds, "CONSOLE", reason);
			}
            else {
				PunishmentManager.punish(RedisBungee.getApi().getUuidFromName(args[0]), "tempmute", seconds, cs.getName(), reason);
			}
        }
    }

	public static class Kick extends Command {
		public Kick(String name) {super(name);}

		public void execute(CommandSender cs, String[] args) {
			if (!Rank.canDoStaff((ProxiedPlayer) cs, RankType2.HELPER, false)) {
				cs.sendMessage(new TextComponent(Punish.no_permission));
				return;
			}

			StringBuilder strBuilder = new StringBuilder();
			for (int i = 1; i <= args.length; i++) {
				strBuilder.append(args[i]);
			}
			String message = strBuilder.toString();
			RedisBungee.getApi().sendProxyCommand("rediskick " + args[0] + " " + message);
		}
	}

	public static class UnBan extends Command {
		public UnBan(String name) {
			super(name);
		}

		public void execute(CommandSender cs, String[] args) {
			if (!Rank.canDoStaff((ProxiedPlayer) cs, RankType2.MOD, false)) {
				cs.sendMessage(new TextComponent(Punish.no_permission));
				return;
			}


			if (BungeeCord.getInstance().getPlayer(args[0]) != null && BungeeCord.getInstance().getPlayer(args[0]).isConnected()) {
				ProxiedPlayer target = BungeeCord.getInstance().getPlayer(args[0]);

				if (!(cs instanceof ProxiedPlayer)) {
					PunishmentManager.unPunish(target.getUniqueId(), "ban");
					cs.sendMessage("§7You have unbanned §a" + target.getName());
				}  else {
					PunishmentManager.unPunish(target.getUniqueId(), "ban");
					cs.sendMessage("§7You have unbanned §a" + target.getName());
				}
			} else {
				cs.sendMessage("§cPlayer not online.");
			}
	}
	}

		public static class UnMute extends Command {
			public UnMute(String name) {super(name);}

			public void execute(CommandSender cs, String[] args) {
				if (!Rank.canDoStaff((ProxiedPlayer) cs, RankType2.HELPER, false)) {
					cs.sendMessage(new TextComponent(Punish.no_permission));
					return;
				}


				if (BungeeCord.getInstance().getPlayer(args[0]) != null && BungeeCord.getInstance().getPlayer(args[0]).isConnected()) {
					ProxiedPlayer target = BungeeCord.getInstance().getPlayer(args[0]);

					if (!(cs instanceof ProxiedPlayer)) {
						PunishmentManager.unPunish(target.getUniqueId(), "mute");
						cs.sendMessage("§7You have unmuted §a" + target.getName());
					} else {
						PunishmentManager.unPunish(target.getUniqueId(), "mute");
						cs.sendMessage("§7You have unmuted §a" + target.getName());
					}
				} else {
					cs.sendMessage("§cPlayer not online.");
				}
			}
		}

	public static class ClearHistory extends Command {
		public ClearHistory(String name) {super(name);}

		public void execute(CommandSender cs, String[] args) {

			if (!Rank.canDoStaff((ProxiedPlayer) cs, RankType2.ADMIN, false)) {
				cs.sendMessage(new TextComponent(Punish.no_permission));
				return;
			}

			if (BungeeCord.getInstance().getPlayer(args[0]) != null && BungeeCord.getInstance().getPlayer(args[0]).isConnected()) {
				ProxiedPlayer target = BungeeCord.getInstance().getPlayer(args[0]);

				if (!(cs instanceof ProxiedPlayer)) {
					PunishmentManager.clearHistory(target.getUniqueId());
					cs.sendMessage("§6History Cleared for " + target.getName());
				} else {
					PunishmentManager.clearHistory(target.getUniqueId());
					cs.sendMessage("§6History Cleared for " + target.getName());
				}
			} else {
				cs.sendMessage("§cPlayer not online.");
			}
		}
	}

    public enum Time {
        SECONDS(1, "second", "seconds", "s", "sec", "secs"),
        MINUTES(60, "minute", "minutes", "m", "min", "mins"),
        HOURS(3600, "hour", "hours", "h", "hr", "hrs"),
        DAYS(86400, "day", "days", "d", "dy", "dys"),
        WEEK(604800, "week", "weeks", "w", "wk", "wks"),
        MONTH(2592000, "month", "months", "m", "mth", "mths"),
        YEAR(31536000, "year", "years", "y", "yr", "yrs");
        private int seconds;
        private String[] match;

        Time(int seconds, String... match) {
            this.seconds = seconds;
            this.match = match;
        }

        public int getSeconds() {
            return seconds;
        }

        public String[] getMatches() {
            return match;
        }

        public static Time getByString(String s) {
            for (Time t : Time.values()) {
                for (String s2 : t.getMatches()) {
                    if (s.toLowerCase().equals(s2)) {
                        System.out.println(s + " " + s2 + t.getSeconds());
                        return t;
                    }
                }
            }
            return null;
        }
    }

    static int getSeconds(String t, String u) {
        Integer time;
        try {
            time = Integer.parseInt(t);
        } catch (NumberFormatException e) {
            return 0;
        }
        Time unit = Time.getByString(u);
        if (unit == null) {
            return -1;
        }
        return time * unit.getSeconds();
    }

    /*public static class unBan extends Command {
        public unBan(String name) {super(name);}
        final private String usage = Main.color("&a/unban <player> &3Moderator rank or above");
        public void execute(CommandSender cs, String[] args) {
            if (!(cs instanceof ProxiedPlayer)) {
                return;
            }
            if (!Rank.canDoStaff((ProxiedPlayer) cs, RankType2.MOD, false)) {
                cs.sendMessage(no_permission);
                return;
            }
            if (args.length == 0) {
                cs.sendMessage(usage);
                return;
            }
            PlayerUtil.getUUID(args[0], new CallBackException<Void, UUID>() {
                @Override
                public Void run(UUID arg) {
                    String uuid = arg.toString();
                    PunishmentManagerOLD.isBanned(arg.toString(), new CallBackException<Void, Boolean>() {
                        @Override
                        public Void run(Boolean arg) {
                            if (arg) {
                                PunishmentManagerOLD.unBan(uuid);
                                cs.sendMessage(Main.color("&aUnbanned &6" + args[0]));
                            } else {
                                cs.sendMessage(Main.color("&c&lProxiedPlayer is not banned on our network"));
                            }
                            return null;
                        }
                        @Override
                        public Void exception(Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    });
                    return null;
                }
                @Override
                public Void exception(Exception e) {
                    if (e instanceof PlayerNotFoundException) {
                        cs.sendMessage(player_not_found);
                    } else {
                        e.printStackTrace();
                    }
                    return null;
                }
            });
            return;
        }
    }
    public static class unMute extends Command {
        public unMute(String name) {super(name);}
        final private String usage = Main.color("&a/unmute <player> &9Helper rank or above");
        public void execute(CommandSender cs, String[] args) {
            if (!(cs instanceof ProxiedPlayer)) {
                return;
            }
            if (!Rank.canDoStaff((ProxiedPlayer) cs, RankType2.HELPER, false)) {
                cs.sendMessage(no_permission);
                return;
            }
            if (args.length == 0) {
                cs.sendMessage(usage);
                return;
            }
            PlayerUtil.getUUID(args[0], new CallBackException<Void, UUID>() {
                @Override
                public Void run(UUID arg) {
                    String uuid = arg.toString();
                    PunishmentManagerOLD.isMuted(arg.toString(), new CallBackException<Void, Boolean>() {
                        @Override
                        public Void run(Boolean arg) {
                            if (arg) {
                                PunishmentManagerOLD.unMute(uuid);
                                cs.sendMessage(Main.color("&aUnmuted &6" + args[0]));
                            } else {
                                cs.sendMessage(Main.color("&c&lProxiedPlayer is not muted on our network"));
                            }
                            if (BungeeCord.getInstance().getPlayer(args[0]) != null) {
                                PunishmentManagerOLD.muted.remove(BungeeCord.getInstance().getPlayer(args[0]).getUniqueId());
                            }
                            return null;
                        }
                        @Override
                        public Void exception(Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    });
                    return null;
                }
                @Override
                public Void exception(Exception e) {
                    if (e instanceof PlayerNotFoundException) {
                        cs.sendMessage(player_not_found);
                    } else {
                        e.printStackTrace();
                    }
                    return null;
                }
            });
            return;
        }
    }
    public static class Check extends Command {
        public Check(String name) {super(name);}
        final private String usage = Main.color("&a/check <player> &9Helper rank or above");
        public void execute(CommandSender cs, String[] args) {
            if (args.length == 0) {
                cs.sendMessage(usage);
                return;
            }
            if (!(cs instanceof ProxiedPlayer)) {
                return;
            }
            if (!Rank.canDoStaff((ProxiedPlayer) cs, RankType2.HELPER, false)) {
                cs.sendMessage(no_permission);
                return;
            }
            PlayerUtil.getUUID(args[0], new CallBackException<Void, UUID>() {
                @Override
                public Void run(UUID arg) {
                    PunishmentManagerOLD.isMuted(arg.toString(), new CallBackException<Void, Boolean>() {
                        @Override
                        public Void run(Boolean arg) {
                            String b = arg ? "is" : "is not";
                            cs.sendMessage(Main.color("&6" + args[0] + "&a " + b + " muted!"));
                            return null;
                        }
                        @Override
                        public Void exception(Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    });
                    PunishmentManagerOLD.isBanned(arg.toString(), new CallBackException<Void, Boolean>() {
                        @Override
                        public Void run(Boolean arg) {
                            String b = arg ? "is" : "is not";
                            cs.sendMessage(Main.color("&6" + args[0] + "&a " + b + " banned!"));
                            return null;
                        }
                        @Override
                        public Void exception(Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    });
                    return null;
                }
                @Override
                public Void exception(Exception e) {
                    if (e instanceof PlayerNotFoundException) {
                        cs.sendMessage(player_not_found);
                    } else {
                        e.printStackTrace();
                    }
                    return null;
                }
            });
            return;
        }
    }*/
}