package net.tntwars.commands.report;

import com.imaginarycode.minecraft.redisbungee.RedisBungee;
import com.imaginarycode.minecraft.redisbungee.RedisBungeeAPI;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.HashMap;
import java.util.UUID;

public class Report
extends Command {
    private HashMap<UUID, Long> time = new HashMap<>();
    private int coolDown = 120000;

    public Report(String name) {
        super(name);
    }
    
	public void execute(CommandSender cs, String[] args) {
        if (cs instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer)cs;
            if (args.length >= 2) {
                RedisBungeeAPI redis = RedisBungee.getApi();

                UUID tid = UUID.fromString(args[0]);

                if (redis.isPlayerOnline(tid)) {
                    if (this.canReport(p.getUniqueId())) {
                        String type = null;
                        if (args[1].contains("hack")) {
                            type = "Hack";
                        } else if (args[1].contains("grief")) {
                            type = "Grief";
                        } else if (args[1].equalsIgnoreCase("chat") || args[1].contains("swear")) {
                            type = "Chat";
                        } else if (args[1].equalsIgnoreCase("other")) {
                            type = "Other";
                        }
                        if (type != null && !type.isEmpty()) {
                            this.time.put(p.getUniqueId(), System.currentTimeMillis());
                            String server = redis.getServerFor(tid).toString();
                            p.sendMessage(new TextComponent("§c§lREPORT §8§l\u00bb §cYour report was submitted!"));
                            String command = "redisreport " + p.getName() + " " + args[0] + " " + server + " " + args[1];
                            redis.sendProxyCommand(command);
                        } else {
                            p.sendMessage(new TextComponent("§c§lREPORT §8§l\u00bb §7Invalid type!"));
                            p.sendMessage(new TextComponent("§c§lREPORT §8§l\u00bb §7Available Types: Hack, Grief, Chat, Other"));
                        }
                    } else {
                        double timeleft = Math.round((this.time.get(p.getUniqueId()) + (long)this.coolDown - System.currentTimeMillis()) / 1000 * 100);
                        p.sendMessage(new TextComponent("§c§lREPORT §8§l\u00bb §7You can report a player again in §e" + String.valueOf(timeleft / 100.0) + " §7seconds!"));
                    }
                } else {
                    p.sendMessage(new TextComponent("§c§lREPORT §8§l\u00bb §7You cannot report §e" + args[0] + " because they are not online§7."));
                }
            } else {
                p.sendMessage(new TextComponent("§c§lREPORT §8§l\u00bb §7/report <player> <type>"));
            }
        }
    }

    private boolean canReport(UUID uuid) {
        if (this.time.containsKey(uuid)) {
            long current = System.currentTimeMillis();
            return this.time.get(uuid) + (long) this.coolDown <= current;
        }
        return true;
    }
}

