package net.tntwars.commands.report;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.tntwars.listeners.PermissionsListener;

import java.util.UUID;

public class RedisReport extends Command {

    public RedisReport(String name) {
        super(name);
    }

    public void execute(CommandSender cs, String[] args) {
        if (args.length >= 4) {
            if (!(cs instanceof ProxiedPlayer)) {
                String reporter = args[0];
                String offender = args[1];
                String server = args[2];
                String type = args[3];

                for (UUID staffUUID : PermissionsListener.onlineStaff) {
                    if(!BungeeCord.getInstance().getPlayer(staffUUID).isConnected()){
                        PermissionsListener.onlineStaff.remove(staffUUID);
                        return;
                    }
                    ProxiedPlayer staffMember = BungeeCord.getInstance().getPlayer(staffUUID);
                    staffMember.sendMessage(new TextComponent("§8§m-----------------------------------------------------"));
                    staffMember.sendMessage(new TextComponent("§8\u00bb §7Report made by: §6" + reporter));
                    staffMember.sendMessage(new TextComponent("§8\u00bb §7Offender: §c" + offender));
                    staffMember.sendMessage(new TextComponent("§8\u00bb §7Server: §c" + server));
                    staffMember.sendMessage(new TextComponent("§8\u00bb §7Type: §c" + type));
                    staffMember.sendMessage(new TextComponent(""));
                    TextComponent st = new TextComponent("§c§l[CLICK TO INVESTIGATE]");
                    st.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§fClick to connect to §e" + server + "§f.").create()));
                    st.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reportserver " + server));
                    staffMember.sendMessage(st);
                    staffMember.sendMessage(new TextComponent("§8§m-----------------------------------------------------"));
                }
            }
        }
    }
}

