package net.tntwars.commands;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class RedisKick extends Command {

    public RedisKick(String name) {
        super(name);
    }

    public void execute(CommandSender sender, String[] args) {
        if (args.length != 0) {
            if (BungeeCord.getInstance().getPlayer(args[0]).isConnected()) {
                ProxiedPlayer target = BungeeCord.getInstance().getPlayer(args[0]);

                StringBuilder strBuilder = new StringBuilder();
                for (int i = 1; i <= args.length; i++) {
                    strBuilder.append(args[i]);
                }
                String message = strBuilder.toString();

                target.disconnect(message);
            }
        }
    }
}