package net.tntwars.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.tntwars.Main;
import net.tntwars.rank.Rank;
import net.tntwars.rank.RankType2;

public class Reload extends Command{

    public Reload(String name) {
        super(name);
    }

    public void execute(CommandSender cs, String[] args) {
        if (cs instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) cs;
            if(Rank.canDoStaff(p, RankType2.DEV, false)){
                Main.reloadConfig();
            }
        } else{
            Main.reloadConfig();
        }
    }
}
