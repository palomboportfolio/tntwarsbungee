package net.tntwars;

public interface CallBackException<R, A> {

    public R run(A arg);
    public R exception(Exception e);
}