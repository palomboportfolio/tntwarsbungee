package net.tntwars;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {


    private final String host, database, user, password;

    public static Connection con;

    public MySQL(String host, String database, String user, String password) {
        this.host= host;
        this.database= database;
        this.user= user;
        this.password= password;

        connect();
    }

    public void connect() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://" + host+ ":3306/" + database+ "?autoReconnect=true",
                    user, password);
            System.out.println("[TNTWARS-BUNGEE] The connection to MySQL is made!");
        } catch (SQLException e) {
            System.out.println("[TNTWARS-BUNGEE] The connection to MySQL couldn't be made! reason: " + e.getMessage());
        }
    }

    public void close() {
        try {
            if (con != null) {
                con.close();
                System.out.println("[TNTWARS-BUNGEE] The connection to MySQL is ended successfully!");
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    public PreparedStatement prepareStatement(String qry) {
        if (!hasConnection()){
            connect();
        }
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(qry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return st;
    }
    public void update(PreparedStatement statement) {
        if (!hasConnection()){
            connect();
        }
        try {
            statement.executeUpdate();
        } catch (SQLException e) {
            connect();
            e.printStackTrace();
        }finally{
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean hasConnection() {
        return con != null;
    }

    public ResultSet query(PreparedStatement statement) {
        if (!hasConnection()){
            connect();
        }
        try {
            return statement.executeQuery();
        } catch (SQLException e) {
            connect();
            e.printStackTrace();
        }
        return null;
    }
}
